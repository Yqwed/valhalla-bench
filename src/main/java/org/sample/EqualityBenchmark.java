package org.sample;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@State(Scope.Benchmark)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class EqualityBenchmark {

    sealed value interface List permits Nil, Cons {}
    value record Nil() implements List {}
    value record Cons(int x, List xs) implements List {}

    @Param({ "1", "2", "5", "10", "50", "100", "500" })
    private int size;

    List startsFrom0 = new Nil();
    List startsFrom1 = new Nil();
    List alsoStartsFrom0 = new Nil();
    List copyStartsFrom0;

    @Setup
    public void setup() {
        for (int i = 0; i < size; ++i) {
            if (i != 0) {
                startsFrom1 = new Cons(i, startsFrom1);
            }
            startsFrom0 = new Cons(i, startsFrom0);
            alsoStartsFrom0 = new Cons(i, alsoStartsFrom0);
        }
        copyStartsFrom0 = startsFrom0;
    }

    @Benchmark
    public boolean nonEqualLists() {
        return startsFrom0 == startsFrom1;
    }

    @Benchmark
    public boolean againstSelf() {
        return startsFrom0 == startsFrom0;
    }

    @Benchmark
    public boolean againstEqual() {
        return startsFrom0 == alsoStartsFrom0;
    }

    @Benchmark
    public boolean againstCopy() {
        return startsFrom0 == copyStartsFrom0;
    }

}

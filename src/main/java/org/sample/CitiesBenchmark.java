/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.sample;

import java.util.concurrent.ThreadLocalRandom;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@State(Scope.Benchmark)
@Threads(4)
@Warmup(iterations = 1)
@Fork(2)
@Measurement(iterations = 1)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
public class CitiesBenchmark {

    primitive record PCity(PPopulation population, PLandArea landArea) {}
    primitive record PPopulation(double population) {}
    primitive record PLandArea(double landArea) {}

    value record VCity(VPopulation population, VLandArea landArea) {}
    value record VPopulation(double population) {}
    value record VLandArea(double landArea) {}
    
    record RCity(RPopulation population, RLandArea landArea) {}
    record RPopulation(double population) {}
    record RLandArea(double landArea) {}

    @Param({"1", "5", "10", "30", "50", "80", "100"})
    public int size;

    PCity[] pCities;
    VCity[] vCities;
    RCity[] rCities;

    double[] populations;
    double[] landAreas;
    double[] flattened;

    @Setup
    public void setup() {
        populations = new double[size];
        landAreas = new double[size];
        flattened = new double[2 * size];
        pCities = new PCity[size];
        vCities = new VCity[size];
        rCities = new RCity[size];

        for (int i = 0; i < size; ++i) {
            populations[i] = ThreadLocalRandom.current().nextInt(1_000_000);
            landAreas[i] = ThreadLocalRandom.current().nextDouble();

            pCities[i] = new PCity(new PPopulation(populations[i]), new PLandArea(landAreas[i]));
            rCities[i] = new RCity(new RPopulation(populations[i]), new RLandArea(landAreas[i]));
            vCities[i] = new VCity(new VPopulation(populations[i]), new VLandArea(landAreas[i]));

            flattened[2 * i] = landAreas[i];
            flattened[2 * i + 1] = populations[i];
        }
    }

    @Benchmark
    public double testAverageDensityPrimitives2Fors() {
        double totalPopulation = 0;
        for (var population : populations) {
            totalPopulation += population;
        }

        double totalArea = 0;
        for (var area : landAreas) {
            totalArea += area;
        }

        return totalPopulation / totalArea;
    }

    @Benchmark
    public double testAverageDensityPrimitives1For() {
        double totalPopulation = 0;
        double totalArea = 0;
        for (int i = 0; i < size; i++) {
            totalPopulation += populations[i];
            totalArea += landAreas[i];
        }

        return totalPopulation / totalArea;
    }

    @Benchmark
    public double testAverageDensityFlattenedPrimitives() {
        double totalArea = 0;
        double totalPopulation = 0;

        for (int i = 0; i < 2 * size; i = i + 2) {
            totalArea += flattened[i];
            totalPopulation += flattened[i + 1];
        }

        return totalPopulation / totalArea;
    }


    @Benchmark
    public double testAverageDensityRecords() {
        double totalArea = 0;
        double totalPopulation = 0;

        for (RCity city : rCities) {
            totalArea += city.landArea().landArea();
            totalPopulation += city.population().population();
        }

        return totalPopulation / totalArea;
    }

    @Benchmark
    public double testAverageDensityValueClasses() {
        double totalArea = 0;
        double totalPopulation = 0;

        for (VCity city : vCities) {
            totalArea += city.landArea().landArea();
            totalPopulation += city.population().population();
        }

        return totalPopulation / totalArea;
    }

    @Benchmark
    public double testAverageDensityPrimitiveClasses() {
        double totalArea = 0;
        double totalPopulation = 0;

        for (PCity city : pCities) {
            totalArea += city.landArea().landArea();
            totalPopulation += city.population().population();
        }

        return totalPopulation / totalArea;
    }

}

# Prerequisites

1. Download valhalla (https://jdk.java.net/valhalla/)
2. Add it to PATH (`export PATH=~/Downloads/jdk-20/bin:$PATH`)
3. `mvn clean verify`
4. `java -XX:+EnablePrimitiveClasses -jar target/benchmarks.jar Cities*`

Results on my PC (Ubuntu 22.04, 5.15.0-52-generic, AMD Ryzen 7 5800X, 32GB):

```
Benchmark                                              (size)  Mode  Cnt   Score   Error  Units
CitiesBenchmark.testAverageDensityFlattenedPrimitives       1  avgt    2   1.809          ns/op
CitiesBenchmark.testAverageDensityFlattenedPrimitives       5  avgt    2   2.557          ns/op
CitiesBenchmark.testAverageDensityFlattenedPrimitives      10  avgt    2   3.710          ns/op
CitiesBenchmark.testAverageDensityFlattenedPrimitives      30  avgt    2   9.628          ns/op
CitiesBenchmark.testAverageDensityFlattenedPrimitives      50  avgt    2  17.131          ns/op
CitiesBenchmark.testAverageDensityFlattenedPrimitives      80  avgt    2  35.137          ns/op
CitiesBenchmark.testAverageDensityFlattenedPrimitives     100  avgt    2  48.223          ns/op
CitiesBenchmark.testAverageDensityPrimitiveClasses          1  avgt    2   1.341          ns/op
CitiesBenchmark.testAverageDensityPrimitiveClasses          5  avgt    2   2.269          ns/op
CitiesBenchmark.testAverageDensityPrimitiveClasses         10  avgt    2   3.753          ns/op
CitiesBenchmark.testAverageDensityPrimitiveClasses         30  avgt    2  10.011          ns/op
CitiesBenchmark.testAverageDensityPrimitiveClasses         50  avgt    2  17.266          ns/op
CitiesBenchmark.testAverageDensityPrimitiveClasses         80  avgt    2  35.047          ns/op
CitiesBenchmark.testAverageDensityPrimitiveClasses        100  avgt    2  48.219          ns/op
CitiesBenchmark.testAverageDensityPrimitives1For            1  avgt    2   1.454          ns/op
CitiesBenchmark.testAverageDensityPrimitives1For            5  avgt    2   2.440          ns/op
CitiesBenchmark.testAverageDensityPrimitives1For           10  avgt    2   3.545          ns/op
CitiesBenchmark.testAverageDensityPrimitives1For           30  avgt    2   9.547          ns/op
CitiesBenchmark.testAverageDensityPrimitives1For           50  avgt    2  17.225          ns/op
CitiesBenchmark.testAverageDensityPrimitives1For           80  avgt    2  35.050          ns/op
CitiesBenchmark.testAverageDensityPrimitives1For          100  avgt    2  48.117          ns/op
CitiesBenchmark.testAverageDensityPrimitives2Fors           1  avgt    2   2.000          ns/op
CitiesBenchmark.testAverageDensityPrimitives2Fors           5  avgt    2   3.030          ns/op
CitiesBenchmark.testAverageDensityPrimitives2Fors          10  avgt    2   4.625          ns/op
CitiesBenchmark.testAverageDensityPrimitives2Fors          30  avgt    2  12.256          ns/op
CitiesBenchmark.testAverageDensityPrimitives2Fors          50  avgt    2  22.839          ns/op
CitiesBenchmark.testAverageDensityPrimitives2Fors          80  avgt    2  44.666          ns/op
CitiesBenchmark.testAverageDensityPrimitives2Fors         100  avgt    2  62.011          ns/op
CitiesBenchmark.testAverageDensityRecords                   1  avgt    2   1.570          ns/op
CitiesBenchmark.testAverageDensityRecords                   5  avgt    2   3.818          ns/op
CitiesBenchmark.testAverageDensityRecords                  10  avgt    2   6.736          ns/op
CitiesBenchmark.testAverageDensityRecords                  30  avgt    2  17.660          ns/op
CitiesBenchmark.testAverageDensityRecords                  50  avgt    2  28.778          ns/op
CitiesBenchmark.testAverageDensityRecords                  80  avgt    2  45.180          ns/op
CitiesBenchmark.testAverageDensityRecords                 100  avgt    2  57.285          ns/op
CitiesBenchmark.testAverageDensityValueClasses              1  avgt    2   1.682          ns/op
CitiesBenchmark.testAverageDensityValueClasses              5  avgt    2   4.070          ns/op
CitiesBenchmark.testAverageDensityValueClasses             10  avgt    2   7.276          ns/op
CitiesBenchmark.testAverageDensityValueClasses             30  avgt    2  18.166          ns/op
CitiesBenchmark.testAverageDensityValueClasses             50  avgt    2  29.602          ns/op
CitiesBenchmark.testAverageDensityValueClasses             80  avgt    2  48.134          ns/op
CitiesBenchmark.testAverageDensityValueClasses            100  avgt    2  60.023          ns/op
```

Assembly for `testAverageDensityFlattenedPrimitives` (`-prof perfasm`)

```
....[Hottest Region 1]..............................................................................
c2, level 4, org.sample.jmh_generated.CitiesBenchmark_testAverageDensityFlattenedPrimitives_jmhTest::testAverageDensityFlattenedPrimitives_avgt_jmhStub, version 7, compile id 770

             0x00007fdf10a663df:   jle    0x00007fdf10a664f8           ;*goto {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
                                                                       ; - org.sample.CitiesBenchmark::testAverageDensityFlattenedPrimitives@44 (line 122)
                                                                       ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityFlattenedPrimitives_jmhTest::testAverageDensityFlattenedPrimitives_avgt_jmhStub@17 (line 232)
             0x00007fdf10a663e5:   mov    $0x2,%edi
             0x00007fdf10a663ea:   mov    %ecx,%ebx
             0x00007fdf10a663ec:   sub    %edi,%ebx
             0x00007fdf10a663ee:   cmp    %edi,%ecx
             0x00007fdf10a663f0:   cmovl  %r9d,%ebx
             0x00007fdf10a663f4:   cmp    $0x1f40,%ebx
             0x00007fdf10a663fa:   cmova  %r11d,%ebx
   0.40%     0x00007fdf10a663fe:   add    %edi,%ebx                    ;*dload_1 {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
                                                                       ; - org.sample.CitiesBenchmark::testAverageDensityFlattenedPrimitives@16 (line 123)
                                                                       ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityFlattenedPrimitives_jmhTest::testAverageDensityFlattenedPrimitives_avgt_jmhStub@17 (line 232)
          ↗  0x00007fdf10a66400:   vaddsd 0x18(%rax,%rdi,8),%xmm1,%xmm1
   9.94%  │  0x00007fdf10a66406:   vaddsd 0x10(%rax,%rdi,8),%xmm2,%xmm2
  10.54%  │  0x00007fdf10a6640c:   vaddsd 0x28(%rax,%rdi,8),%xmm1,%xmm1
  11.14%  │  0x00007fdf10a66412:   vaddsd 0x20(%rax,%rdi,8),%xmm2,%xmm2
  11.45%  │  0x00007fdf10a66418:   vaddsd 0x38(%rax,%rdi,8),%xmm1,%xmm1
  10.54%  │  0x00007fdf10a6641e:   vaddsd 0x30(%rax,%rdi,8),%xmm2,%xmm2
  12.55%  │  0x00007fdf10a66424:   vaddsd 0x48(%rax,%rdi,8),%xmm1,%xmm1;*dadd {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
          │                                                            ; - org.sample.CitiesBenchmark::testAverageDensityFlattenedPrimitives@36 (line 124)
          │                                                            ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityFlattenedPrimitives_jmhTest::testAverageDensityFlattenedPrimitives_avgt_jmhStub@17 (line 232)
  10.54%  │  0x00007fdf10a6642a:   vaddsd 0x40(%rax,%rdi,8),%xmm2,%xmm2;*dadd {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
          │                                                            ; - org.sample.CitiesBenchmark::testAverageDensityFlattenedPrimitives@24 (line 123)
          │                                                            ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityFlattenedPrimitives_jmhTest::testAverageDensityFlattenedPrimitives_avgt_jmhStub@17 (line 232)
  10.54%  │  0x00007fdf10a66430:   add    $0x8,%edi                    ;*iadd {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
          │                                                            ; - org.sample.CitiesBenchmark::testAverageDensityFlattenedPrimitives@41 (line 122)
          │                                                            ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityFlattenedPrimitives_jmhTest::testAverageDensityFlattenedPrimitives_avgt_jmhStub@17 (line 232)
   1.00%  │  0x00007fdf10a66433:   cmp    %ebx,%edi
          ╰  0x00007fdf10a66435:   jl     0x00007fdf10a66400           ;*if_icmpge {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
                                                                       ; - org.sample.CitiesBenchmark::testAverageDensityFlattenedPrimitives@13 (line 122)
                                                                       ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityFlattenedPrimitives_jmhTest::testAverageDensityFlattenedPrimitives_avgt_jmhStub@17 (line 232)
             0x00007fdf10a66437:   mov    0x388(%r15),%rbx             ; ImmutableOopMap {r10=Oop rdx=Oop rax=Oop r14=Oop xmm0=Oop }
                                                                       ;*goto {reexecute=1 rethrow=0 return_oop=0 return_scalarized=0}
                                                                       ; - (reexecute) org.sample.CitiesBenchmark::testAverageDensityFlattenedPrimitives@44 (line 122)
                                                                       ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityFlattenedPrimitives_jmhTest::testAverageDensityFlattenedPrimitives_avgt_jmhStub@17 (line 232)
             0x00007fdf10a6643e:   test   %eax,(%rbx)                  ;*goto {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
....................................................................................................
  88.65%  <total for region 1>

```

Assembly for `testAverageDensityPrimitiveClasses`(`-prof perfasm`)

```
....[Hottest Region 1]..............................................................................
c2, level 4, org.sample.jmh_generated.CitiesBenchmark_testAverageDensityPrimitiveClasses_jmhTest::testAverageDensityPrimitiveClasses_avgt_jmhStub, version 4, compile id 777

             0x00007f2100a66d38:   cmova  %r10d,%r11d
             0x00007f2100a66d3c:   add    %ecx,%r11d
             0x00007f2100a66d3f:   nop                                 ;*aload {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
                                                                       ; - org.sample.CitiesBenchmark::testAverageDensityPrimitiveClasses@25 (line 162)
                                                                       ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityPrimitiveClasses_jmhTest::testAverageDensityPrimitiveClasses_avgt_jmhStub@17 (line 232)
          ↗  0x00007f2100a66d40:   movslq %ecx,%r13
          │  0x00007f2100a66d43:   shl    $0x4,%r13                    ;*aaload {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
          │                                                            ; - org.sample.CitiesBenchmark::testAverageDensityPrimitiveClasses@29 (line 162)
          │                                                            ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityPrimitiveClasses_jmhTest::testAverageDensityPrimitiveClasses_avgt_jmhStub@17 (line 232)
          │  0x00007f2100a66d47:   vaddsd 0x10(%rax,%r13,1),%xmm2,%xmm2
   5.53%  │  0x00007f2100a66d4e:   vaddsd 0x18(%rax,%r13,1),%xmm1,%xmm1
   5.15%  │  0x00007f2100a66d55:   vaddsd 0x20(%rax,%r13,1),%xmm2,%xmm2
   4.85%  │  0x00007f2100a66d5c:   vaddsd 0x28(%rax,%r13,1),%xmm1,%xmm1
   5.83%  │  0x00007f2100a66d63:   vaddsd 0x30(%rax,%r13,1),%xmm2,%xmm2
   5.34%  │  0x00007f2100a66d6a:   vaddsd 0x38(%rax,%r13,1),%xmm1,%xmm1
   5.63%  │  0x00007f2100a66d71:   vaddsd 0x40(%rax,%r13,1),%xmm2,%xmm2
   6.31%  │  0x00007f2100a66d78:   vaddsd 0x48(%rax,%r13,1),%xmm1,%xmm1
   4.85%  │  0x00007f2100a66d7f:   vaddsd 0x50(%rax,%r13,1),%xmm2,%xmm2
   5.53%  │  0x00007f2100a66d86:   vaddsd 0x58(%rax,%r13,1),%xmm1,%xmm1
   5.53%  │  0x00007f2100a66d8d:   vaddsd 0x60(%rax,%r13,1),%xmm2,%xmm2
   5.53%  │  0x00007f2100a66d94:   vaddsd 0x68(%rax,%r13,1),%xmm1,%xmm1
   5.92%  │  0x00007f2100a66d9b:   vaddsd 0x70(%rax,%r13,1),%xmm2,%xmm2
   5.05%  │  0x00007f2100a66da2:   vaddsd 0x78(%rax,%r13,1),%xmm1,%xmm1
   5.73%  │  0x00007f2100a66da9:   vaddsd 0x80(%rax,%r13,1),%xmm2,%xmm2;*dadd {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
          │                                                            ; - org.sample.CitiesBenchmark::testAverageDensityPrimitiveClasses@52 (line 164)
          │                                                            ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityPrimitiveClasses_jmhTest::testAverageDensityPrimitiveClasses_avgt_jmhStub@17 (line 232)
   4.76%  │  0x00007f2100a66db3:   vaddsd 0x88(%rax,%r13,1),%xmm1,%xmm1;*dadd {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
          │                                                            ; - org.sample.CitiesBenchmark::testAverageDensityPrimitiveClasses@41 (line 163)
          │                                                            ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityPrimitiveClasses_jmhTest::testAverageDensityPrimitiveClasses_avgt_jmhStub@17 (line 232)
   5.34%  │  0x00007f2100a66dbd:   add    $0x8,%ecx                    ;*iinc {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
          │                                                            ; - org.sample.CitiesBenchmark::testAverageDensityPrimitiveClasses@54 (line 162)
          │                                                            ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityPrimitiveClasses_jmhTest::testAverageDensityPrimitiveClasses_avgt_jmhStub@17 (line 232)
   0.10%  │  0x00007f2100a66dc0:   cmp    %r11d,%ecx
          ╰  0x00007f2100a66dc3:   jl     0x00007f2100a66d40           ;*if_icmpge {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
                                                                       ; - org.sample.CitiesBenchmark::testAverageDensityPrimitiveClasses@22 (line 162)
                                                                       ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityPrimitiveClasses_jmhTest::testAverageDensityPrimitiveClasses_avgt_jmhStub@17 (line 232)
             0x00007f2100a66dc9:   mov    0x388(%r15),%r11             ; ImmutableOopMap {rdi=Oop rdx=Oop rax=Oop r14=Oop xmm0=Oop }
                                                                       ;*goto {reexecute=1 rethrow=0 return_oop=0 return_scalarized=0}
                                                                       ; - (reexecute) org.sample.CitiesBenchmark::testAverageDensityPrimitiveClasses@57 (line 162)
                                                                       ; - org.sample.jmh_generated.CitiesBenchmark_testAverageDensityPrimitiveClasses_jmhTest::testAverageDensityPrimitiveClasses_avgt_jmhStub@17 (line 232)
             0x00007f2100a66dd0:   test   %eax,(%r11)                  ;*goto {reexecute=0 rethrow=0 return_oop=0 return_scalarized=0}
....................................................................................................
  86.99%  <total for region 1>                                                                       
```